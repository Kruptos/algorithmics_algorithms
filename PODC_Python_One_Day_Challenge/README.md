# Python One Day Challenge

This branch has everything needed to make the Challenge

The "exhibition ordering.pdf" file explains what is needed to do for this challenge
The "score_checker.py" script is given to help participants to evaluate the score of their files

5 files are given, each one of them needing to be ordered:

0_example.txt
1_binary_landscapes.txt
10_computable_moments.txt
11_randomizing_paintings.txt
110_oily_portraits.txt

The first file, 0_example.txt is a small file and it is easy to use it to experiment.
0_example.txt:

************************************************************************************************************************************************

4

L 3 animals fear war

P 2 smile woman

P 2 woman pearl

L 2 fear raft survivors


************************************************************************************************************************************************


How to use the (proposed) Python PODC.py script:

*python parser.py nom_fichier_input nom_fichier_output  --opt option*

With 3 possible choices for options:

--opt easy => it just uses the "natural order" (i.e. the order in the file)

--opt size => it orders the arrays with the length of the tags set

--opt rand => it orders the arrays randomly
